import { createApp } from "vue";
import App from "./App.vue";
import mdiVue from "mdi-vue/v3";
import * as mdijs from "@mdi/js";
import axios from "axios";
import VueAxios from "vue-axios";

import "./assets/style.scss";

const app = createApp(App);
app.use(VueAxios, axios);
app.use(mdiVue, { icons: mdijs });
app.mount("#app");
