export default [
  {
    title: "Категория",
    prop: "title",
    totalTitle: "Итого",
    totalProp: null,
    notFilter: true,
  },
  {
    title: "👀",
    prop: "countView",
    totalProp: "countView",
  },
  {
    title: "▶️",
    prop: "countPlay",
    totalProp: "countPlay",
  },
  {
    title: "⬆️",
    prop: "countSend",
    totalProp: "countSend",
  },
  {
    title: "📤",
    prop: "countOrder",
    totalProp: "countOrder",
  },
  {
    title: "💳",
    prop: "countPay",
    totalProp: "countPay",
  },
  {
    title: "👀/⬆️",
    prop: "viewSend",
    totalProp: "viewSend",
    precent: true,
  },
  {
    title: "👀/💳",
    prop: "viewPay",
    totalProp: "viewPay",
    precent: true,
  },
];
